# data-attribution-feed

Given a list of location groups, with each group assigned a radius in KM, finds all devices within each group&#39;s location items&#39; radius.